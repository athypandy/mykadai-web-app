package com.example.mykadai;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

public class Bakery extends AppCompatActivity {
    WebView mywebview;
    ImageView ivhome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bakery);
        ivhome=findViewById(R.id.ivhome);
        mywebview = (WebView) findViewById(R.id.webview);
        ivhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Bakery.this, MainActivity.class);
                startActivity(intent);
            }
        });
        WebSettings webSettings = mywebview.getSettings();
        mywebview.getSettings().setDomStorageEnabled(true);
        mywebview.getSettings().setJavaScriptEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        mywebview.setWebViewClient(new WebViewClient());
        mywebview.requestFocus();
        mywebview.getSettings().setBuiltInZoomControls(true);
        mywebview.getSettings().setDisplayZoomControls(false);
        String deviceid= Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
        mywebview.loadUrl("http://136.233.82.211/default/mykadaicategory/page/view?categoryid=13&deviceid="+deviceid);

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mywebview.canGoBack()) {
                        mywebview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
}