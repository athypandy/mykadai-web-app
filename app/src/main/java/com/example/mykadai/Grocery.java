package com.example.mykadai;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

public class Grocery extends AppCompatActivity {
    WebView mywebview;
    ImageView ivhome;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grocery);
        ivhome=findViewById(R.id.ivhome);
        mywebview = (WebView) findViewById(R.id.webview);
        ivhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Grocery.this, MainActivity.class);
                startActivity(intent);
            }
        });
        WebSettings webSettings = mywebview.getSettings();
        mywebview.getSettings().setDomStorageEnabled(true);
        mywebview.getSettings().setJavaScriptEnabled(true);
        webSettings.setJavaScriptEnabled(true);
        mywebview.setWebViewClient(new WebViewClient());
        mywebview.requestFocus();
        mywebview.getSettings().setBuiltInZoomControls(true);
        mywebview.getSettings().setDisplayZoomControls(false);
        String deviceid= Settings.Secure.getString(getContentResolver(),Settings.Secure.ANDROID_ID);
        mywebview.loadUrl("http://136.233.82.211/default/mykadaicategory/page/view?categoryid=3&deviceid="+deviceid);
        //"http://136.233.82.211/default/mykadaicategory/page/view?categoryid=9&deviceid=985698"
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mywebview.canGoBack()) {
                        mywebview.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }
}
