package com.example.mykadai;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.PendingDynamicLinkData;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;

public class AddCard extends AppCompatActivity {
    ImageView ivhome;
    Button btn;
    String TAG;
    WebView mywebview;
    String uri, finaluri, trxt, sss;
    Uri dynamicLinkUri;
    final int UPI_PAYMENT = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        ivhome=findViewById(R.id.ivhome);
        ivhome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddCard.this, MainActivity.class);
                startActivity(intent);
            }
        });
        mywebview = (WebView) findViewById(R.id.webviewcart);
        WebSettings webSettings = mywebview.getSettings();
        mywebview.getSettings().setDomStorageEnabled(true);
        mywebview.getSettings().setJavaScriptEnabled(true);
        mywebview.getSettings().setLoadWithOverviewMode(true);
        mywebview.getSettings().setUseWideViewPort(true);
        webSettings.setJavaScriptEnabled(true);
        mywebview.requestFocus();
        mywebview.getSettings().setRenderPriority(WebSettings.RenderPriority.HIGH);
        if (Build.VERSION.SDK_INT >= 19) {
            mywebview.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            mywebview.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }
        mywebview.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        mywebview.getSettings().setBuiltInZoomControls(true);
        mywebview.getSettings().setDisplayZoomControls(false);
        FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnSuccessListener(this, new OnSuccessListener<PendingDynamicLinkData>() {
                    @Override
                    public void onSuccess(PendingDynamicLinkData pendingDynamicLinkData) {
                        // Get deep link from result (may be null if no link is found)
                        dynamicLinkUri = null;
                        if (pendingDynamicLinkData != null) {
                            dynamicLinkUri = pendingDynamicLinkData.getLink();
                            System.out.println("dyanamic :" + dynamicLinkUri);
                            uri = dynamicLinkUri.getQueryParameter("OrderId");
                            uri = dynamicLinkUri.getQueryParameter("Restid");
                            uri = dynamicLinkUri.getQueryParameter("orderid");
                            uri = dynamicLinkUri.getQueryParameter("totalamount");
                            uri = dynamicLinkUri.getQueryParameter("restaurantid");
                        }
                        if (dynamicLinkUri != null) {
                            uri = dynamicLinkUri.toString();
                            try {
                                finaluri = URLDecoder.decode(uri, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                e.printStackTrace();
                            }
                            mywebview.loadUrl(uri);
                            System.out.println("i am here");
                            System.out.println("Link :" + uri);
                        }
                    }
                });
        mywebview.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView wv, String url) {
                if (URLUtil.isNetworkUrl(url)) {
                    return false;
                }
                if (url.startsWith("upi:")) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setType("text/plain");
                    intent.setData(Uri.parse(url));
                    Intent chooser = Intent.createChooser(intent, "Pay with...");
                    startActivityForResult(chooser, UPI_PAYMENT, null);
                    return true;
                } else {
                    Toast.makeText(AddCard.this, "Kindly Install Any UPI APP", Toast.LENGTH_LONG).show();
                }
                return true;
            }
        });
    }
    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       //  System.out.println("onactivityresult::requestcode" + " " + requestCode + " " + "resultcodeee" + " " + resultCode + " " + "intentee" + " " + data);
        switch (requestCode) {
            case UPI_PAYMENT:
                if ((RESULT_OK == resultCode) || (resultCode == 11)) {

                    if (data != null) {
                        trxt = data.getStringExtra("response");
                        ArrayList<String> dataList = new ArrayList<>();
                        dataList.add(trxt);
                        sss = dataList.toString();
                        System.out.println("ans 1:"+sss);
                        Toast.makeText(getApplicationContext(), trxt, Toast.LENGTH_LONG).show();
                        mywebview.evaluateJavascript("javascript:myTestFunction(\"" + trxt + "\");", null);
                    //    System.out.println("web 1 :"+ mywebview);

                    } else {
                        trxt = "nothing";
                        ArrayList<String> dataList = new ArrayList<>();
                        dataList.add("nothing");
                        sss = dataList.toString();
                        System.out.println("ans 2:"+sss);
                        Toast.makeText(getApplicationContext(), trxt, Toast.LENGTH_SHORT).show();
                        mywebview.evaluateJavascript("javascript:myTestFunction(\"" + trxt + "\");", null);
                       // System.out.println("web 2 :"+ mywebview);

                    }
                } else {
                    trxt = "nothing";
                    ArrayList<String> dataList = new ArrayList<>();
                    dataList.add("nothing");
                    sss = dataList.toString();
                    System.out.println("ans 4:"+sss);
                    Toast.makeText(getApplicationContext(), trxt, Toast.LENGTH_SHORT).show();
                    mywebview.evaluateJavascript("javascript:myTestFunction(\"" + trxt + "\");", null);
                  // System.out.println("web 3 :"+ mywebview);

                }
                break;
        }

    }
}

